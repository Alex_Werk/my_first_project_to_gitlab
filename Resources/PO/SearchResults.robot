*** Settings ***
Library  SeleniumLibrary

*** Variables ***


*** Keywords ***
Search for Products
    Log  Search for products in Search Results
    Enter Search Term
    Submit Search
    Access Link

Enter Search Term
    Input Text  id=searchfor  Lego Bugatti
    Sleep  3s

Submit Search
    Click Button  xpath=//*[@id="siteSearch"]/div/button

Access Link
    Sleep  3s
    Click Link  css=#js_items_content > li:nth-child(2) > div.product-item__content > div > div.product-title--inline > a