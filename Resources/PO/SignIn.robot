*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${POPUP_MESSAGE} =  Het artikel is toegevoegd aan je winkelwagen
${LINK_ORDER_BUTTON} =  css=body > div.js_modal_wrapper.modal.modal--small > div.modal__window > div.basket-popup.js_narrow_modal_window > div.fluid-grid.fluid-grid--middle.fluid-grid--xs > div:nth-child(2) > div > a

*** Keywords ***
Add item to Cart
    Click Link  xpath=//*[@id="9200000091824251"]
    Sleep  3s


Continue to Checkout
    Wait Until Page Contains  ${POPUP_MESSAGE}
    Sleep  3s
    Click Link  ${LINK_ORDER_BUTTON}
    Sleep  3s

Sign in to Checkout
    Click Button  css=#continue_ordering_bottom
    Sleep  3s