*** Settings ***
Library  SeleniumLibrary
Resource  ./PO/LandingPage.robot
Resource  ./PO/SearchResults.robot
Resource  ./PO/SignIn.robot

*** Variables ***


*** Keywords ***
Search for Products
    SearchResults.Search for Products

Go to Landing Page
    LandingPage.Navigate To
    LandingPage.Verify Cookie Page
    LandingPage.Verify Page Loaded

Sign in to checkout
    SignIn.Add item to Cart
    SignIn.Continue to Checkout
    SignIn.Sign in to Checkout
