*** Settings ***
Documentation  Zoeken naar een artikel bij bol.com en inloggen om te bestellen
Resource  ../Resources/Common.robot
Resource  ../Resources/TestApp.robot
Test Setup  Begin Web Test
Test Teardown  End Web Test

#robot -d results tests/SearchShop.robot
#robot -d results -i Intake tests

*** Variables ***
${BROWSER}=  gc
${URL} =  http://www.bol.com

*** Test Cases ***
Should be able to access Page
    [Documentation]  Toegang tot pagina
    [Tags]  Intake
    TestApp.Go to Landing Page

User can search for products
    [Documentation]  Zoeken op pagina
    [Tags]  Zoek
    TestApp.Go to Landing Page
    TestApp.Search for Products


User must sign in to check out
    [Documentation]  Inloggen om te bestellen
    [Tags]  Inloggen
    TestApp.Go to Landing Page
    TestApp.Search for Products
    TestApp.Sign in to Checkout



